FROM alpine:3.17.2
LABEL authors="aartamonov"

RUN apk add openjdk11
COPY build/libs/cats-api.jar /app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]